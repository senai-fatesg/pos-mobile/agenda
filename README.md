# Agenda

## URL do Swagger

http://localhost:8080/agenda/swagger-ui.html

## Demais informações/endereços

* Eclipse: http://franciscocalaca.com/eclipse.zip
* DBeaver: https://dbeaver.io/download/
* Insomnia: https://updates.insomnia.rest/downloads/windows/latest?app=com.insomnia.app&ref=https%3A%2F%2Falternativeto.net%2Fsoftware%2Finsomnia-rest-client%2Fabout%2F
* Postgres: https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
* URL do start spring: https://start.spring.io/
* Properties de configurações: https://gitlab.com/senai-fatesg/pos-mobile/agenda/-/blob/master/src/main/resources/application.properties

### Na instalação do postgres

Desmarque a opção StackBuilder

![](/doc/postgres1.png)

Utilize as credenciais

```
usuário: postgres
senha:   123456
```

### Para executar o projeto vá em:

![](/doc/executarProjeto.jpeg)