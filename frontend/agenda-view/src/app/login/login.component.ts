import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = "";
  password = "";

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
  }

  login(){
    this.loginService.login(this.user, this.password).subscribe(r => {
      localStorage.setItem('access_token_posmobile', r.access_token);
      this.router.navigate(['/']);
      console.log(r);

    });
  }

}
