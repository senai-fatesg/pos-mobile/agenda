import { Component, OnInit } from '@angular/core';
import { ContactService } from '../services/contact.service';

@Component({
  selector: 'app-contact-create',
  templateUrl: './contact-create.component.html',
  styleUrls: ['./contact-create.component.css']
})
export class ContactCreateComponent implements OnInit {

  contact: {id, name, email, description} = {id: null, name: "", email: "", description: ""};

  constructor(private dataService: ContactService) { }

  ngOnInit() {
  }

  createContact(){
    console.log(this.contact);
    this.dataService.createContact(this.contact).subscribe(r => {
      this.contact = {id: null, name: "", email: "", description: ""};
    });
  }

}
