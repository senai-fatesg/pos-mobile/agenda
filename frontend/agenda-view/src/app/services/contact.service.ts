import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http: HttpClient) { }

  public getContacts():Observable<any>{
    return this.http.get(`${environment.url}/rest/contact`);
  }
  public createContact(contact: {id, name, description, email}){
    return this.http.post(`${environment.url}/rest/contact`, contact);  
  }

}
