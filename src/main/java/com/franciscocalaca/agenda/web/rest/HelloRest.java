package com.franciscocalaca.agenda.web.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloRest {

	@GetMapping
	public String retornarOk() {
		return "OK. estou funcionando!";
	}

	@GetMapping("/outro")
	public String outro() {
		return "OUTRO WEB SERVICE";
	}
}
