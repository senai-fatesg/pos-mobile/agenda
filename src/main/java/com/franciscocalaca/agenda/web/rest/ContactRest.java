package com.franciscocalaca.agenda.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.franciscocalaca.agenda.dao.ContactDao;
import com.franciscocalaca.agenda.model.Contact;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/contact")
public class ContactRest {

	@Autowired
	private ContactDao contatoDao;
	
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Retorna a lista de contatos"),
		    @ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"),
		    @ApiResponse(code = 500, message = "Foi gerada uma exceção no servidor"),
		})
	@GetMapping
	public List<Contact> get(){
		return contatoDao.findAll();
	}
	
	@PostMapping
	public void post(@RequestBody Contact contato) {
		contatoDao.save(contato);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") Integer id) {
		contatoDao.deleteById(id);
	}
	
	@PutMapping
	public void put(@RequestBody Contact contato) {
		contatoDao.save(contato);
	}
}
