package com.franciscocalaca.agenda.web.rest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.franciscocalaca.http.utils.Token;
import com.franciscocalaca.http.utils.UtilHttp;

@Service
public class AuthTokenService {
	
	@Value("${agenda.auth.url}")
	private String url;
	
	@Value("${agenda.auth.clientId}")
	private String clientId;
	
	@Value("${agenda.auth.clientSecret}")
	private String clientSecret;

	public Token getToken(String user, String password) {
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Authorization", UtilHttp.getAuthHeaderBase64(clientId, clientSecret));
		
		Map<String, String> parameters = new HashMap<>();
		parameters.put("grant_type", "password");
		
		try {
			return new Token(url, user, password, headers, parameters);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
