package com.franciscocalaca.agenda.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.franciscocalaca.agenda.dao.ProductDao;
import com.franciscocalaca.agenda.model.Product;

@RestController
@RequestMapping("/rest/product")
public class ProductRest {

	@Autowired
	private ProductDao productDao;
	
	@GetMapping
	public List<Product> get(){
		return productDao.findAll();
	}
	
	@PostMapping
	public void post(@RequestBody Product contato) {
		productDao.save(contato);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") Long id) {
		productDao.deleteById(id);
	}
	
	@PutMapping
	public void put(@RequestBody Product contato) {
		productDao.save(contato);
	}
}
