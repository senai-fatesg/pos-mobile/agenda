package com.franciscocalaca.agenda.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.franciscocalaca.agenda.model.Contact;

/*
 * DAO - Data Access Object
 */
@Repository
public interface ContactDao extends JpaRepository<Contact, Integer>{

	
	
}
