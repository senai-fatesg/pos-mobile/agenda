package com.franciscocalaca.agenda.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.franciscocalaca.agenda.model.Product;

/*
 * DAO - Data Access Object
 */
@Repository
public interface ProductDao extends JpaRepository<Product, Long>{

	
	
}
