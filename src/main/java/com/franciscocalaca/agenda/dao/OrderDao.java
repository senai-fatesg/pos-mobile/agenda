package com.franciscocalaca.agenda.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.franciscocalaca.agenda.model.Order;

/*
 * DAO - Data Access Object
 */
@Repository
public interface OrderDao extends JpaRepository<Order, Integer>{

	
	
}
